


all: ruby-version hello hello-by-textfile ciudades

ruby-version:
	ruby -e "puts RUBY_VERSION"

hello: 
	ruby hello.rb

hello-by-textfile:
	ruby -F'\n' -00 -ane 'print if $$F.length==1' sample.txt

ciudades:
	ruby ciudades.rb
