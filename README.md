# Pruebas con Ruby


# Algunas cosas utiles
- `ruby -h` for summary of options
- explainshell - to quickly get information without having to traverse through the docs
- ruby-lang documentation - manuals, tutorials and references
- A bunch of Ruby one liners. From multiline processing to array operations and dealing with duplicates. https://github.com/learnbyexample/Command-line-text-processing/blob/master/ruby_one_liners.md
- Lista de proyectos open source en Rails http://www.opensourcerails.com/
    - http://opensourcebilling.org/
    http://www.fatfreecrm.com/
    https://jekyllrb.com/
- https://github.com/dreikanter/ruby-bookmarks
- https://pine.fm/LearnToProgram/
